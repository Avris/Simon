export class Reaction {
    constructor(filename, display, type) {
        this.display = display;
        this.searchableName = this.getSearchableName(this.display)
        this.type = type;
        this.filename = filename;
    }

    matches(filter, type) {
        if (filter && !this.searchableName.includes(this.getSearchableName(filter))) {
            return false;
        }

        if (type && this.type !== type) {
            return false;
        }

        return true;
    }

    getMark() {
        return Reaction.TYPES[this.type];
    }

    getSearchableName(s) {
        return s.trim().replace(/[.…,\/#!?$%\^&\*;:{}=\-_`'’"~()]/g,"").toLowerCase()
    }

    static get TYPES() {
        return {
            Positive: 'success',
            Neutral: 'warning',
            Negative: 'danger',
        }
    }
}
