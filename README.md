# SimonSays

## Build Setup

```bash
# install dependencies
$ make install

# serve with hot reload at localhost:3000
$ make run

# generate static project
$ make deploy
```
