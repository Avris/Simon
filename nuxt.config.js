const title = 'Simon Says'
const description = 'A reaction board tribute to Simon Anthony from Cracking The Cryptic.'
const base = 'https://simonsays.avris.it'
const banner = base + '/banner.png'
const colour = '#0391af';
const locale = 'en';

export default {
    target: 'static',
    head: {
        title: title,
        meta: [
            { charset: 'utf-8' },

            { hid: 'description', name: 'description', content: description },

            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'apple-mobile-web-app-title', name: 'apple-mobile-web-app-title', content: title },
            { hid: 'theme-color', name: 'theme-color', content: colour },

            { hid: 'og:type', property: 'og:type', content: 'article' },
            { hid: 'og:title', property: 'og:title', content: title },
            { hid: 'og:description', property: 'og:description', content: description },
            { hid: 'og:site_name', property: 'og:site_name', content: title },
            { hid: 'og:image', property: 'og:image', content: banner },

            { hid: 'twitter:card', property: 'twitter:card', content: 'summary_large_image' },
            { hid: 'twitter:title', property: 'twitter:title', content: title },
            { hid: 'twitter:description', property: 'twitter:description', content: description },
            { hid: 'twitter:site', property: 'twitter:site', content: base },
            { hid: 'twitter:image', property: 'twitter:image', content: banner },
        ],
        link: [
            { rel: 'icon', type: 'image/png', href: '/favicon.png' }
        ]
    },
    components: true,
    pwa: {
        manifest: {
            name: title,
            short_name: title,
            description: description,
            background_color: '#ffffff',
            theme_color: colour,
            lang: locale,
        }
    },
    modules: [
        // https://go.nuxtjs.dev/pwa
        '@nuxtjs/pwa',
        'vue-plausible',
    ],
    plausible: {
        domain: 'simonsays.avris.it',
        apiHost: 'https://plausible.avris.it',
    },
}
