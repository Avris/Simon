install:
	pnpm install

run:
	pnpm run dev

deploy: install
	pnpm run generate
